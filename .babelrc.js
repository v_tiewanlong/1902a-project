const plugins = [['@babel/plugin-proposal-decorators', { legacy: true }]];
const config = {
  presets: ['react-app'],
  plugins,
};
if (process.env.NODE_ENV === 'production') {
  //生成环境移除console.log
  plugins.push('babel-plugin-transform-remove-console');
}

module.exports = config;

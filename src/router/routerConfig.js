// import Index from '../views/index/index'
import React, { Component, lazy } from 'react';
import IsLogin from '@/component/islogin/islogin';

// const loadable = (ck)=>{
//     return class extends  Component{
//         state = {
//             Com:null
//         }
//         componentDidMount(){
//             ck().then(res=>{
//                 console.log(res,'res')
//                 this.setState({
//                     Com:res.default
//                 })
//             })
//         }
//         render() {
//             const {Com} = this.state;
//             return Com && <Com />
//         }
//     }
// }
// loabable({loader:,loading})

export const IndexRoutes = [
  {
    path: '/index/article',
    meta: {
      title: '文章',
      isAuth: IsLogin,
    },
    component: lazy(() => import('../views/index/article/article')),
  },
  {
    path: '/index/file',
    meta: {
      title: '归档',
      isAuth: IsLogin,
    },
    component: lazy(() => import('../views/index/file/file')),
  },
  {
    path: '/index/knowledge',
    meta: {
      title: '知识小册',
    },
    component: lazy(() => import('../views/index/knowledge/knowledge')),
  },
];
const routes = {
  // mode: 'hash',
  routes: [
    {
      path: '/index',
      component: lazy(() => import('../views/index/index')),
      children: [
        ...IndexRoutes,
        {
          path: '*',
          component: lazy(() => import('../views/notfound/notfound')),
        },
        {
          path: '/index',
          to: '/index/article',
        },
      ],
    },
    {
      path: '/',
      to: '/index',
    },
    {
      path: '/login',
      component: lazy(() => import('../views/login/login')),
    },
    {
      path: '*',
      component: lazy(() => import('../views/notfound/notfound')),
    },
  ],
};

export default routes;

import React, { Component, Suspense } from 'react';
import { BrowserRouter, Switch, Route, HashRouter } from 'react-router-dom';
import routerConfig from './routerConfig';
import RouterView from './routerview';
function RootRouter() {
  const { mode = 'history', routes } = routerConfig;
  return mode == 'history' ? (
    <BrowserRouter>
      <Suspense fallback={<div>loading...</div>}>
        <RouterView routes={routes} />
      </Suspense>
    </BrowserRouter>
  ) : (
    <HashRouter>
      <Suspense fallback={<div>loading...</div>}>
        <RouterView routes={routes} />
      </Suspense>
    </HashRouter>
  );
}

export default RootRouter;

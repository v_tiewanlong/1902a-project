import React, { Component } from 'react';
import file from './file.module.less';
import IsLogin from '@/component/islogin/islogin';
class File extends Component {
  state = {
    title: '1902a',
  };
  componentDidMount() {
    //setTimeOut和原生事件里面setState是同步的
    this.timers = setTimeout(() => {
      this.setState({
        title: '1903a',
      });
      console.log(this.state.title);
    }, 0);
  }
  componentWillUnmount() {
    clearTimeout(this.timers);
  }
  handleClick = () => {
    let { title } = this.state;
    this.setState(
      {
        title: '1903a',
      },
      () => {}
    );
    console.log(title, 'title');
  };
  render() {
    const { title } = this.state;
    return (
      <div className={file.container}>
        <h3>{title}</h3>
        <button onClick={this.handleClick}>点击</button>
        <p id="btns"></p>
      </div>
    );
  }
}

export default File;

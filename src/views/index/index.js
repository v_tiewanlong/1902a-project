import React, { Component } from 'react';
import RouterView from '../../router/routerview';
import index from './index.module.less';
import classnames from 'classnames';
import { IndexRoutes } from '@/router/routerConfig';
import { Menu } from 'antd';
import { NavLink } from 'react-router-dom';
import Theme from '@/component/theme/theme';
import connect from '@/utils/connect.js';
console.log(index);
class Index extends Component {
  render() {
    const { child, flag } = this.props;
    return (
      <div>
        <header className={index.header}>
          <Menu
            className={index.menus}
            theme={flag ? 'light' : 'drak'}
            mode="horizontal"
            defaultSelectedKeys={['2']}
          >
            {IndexRoutes.map((item, index) => {
              return (
                <Menu.Item key={index}>
                  <NavLink to={item.path}>{`${item.meta.title}`}</NavLink>
                </Menu.Item>
              );
            })}
          </Menu>

          <Theme />
        </header>
        <section className={classnames('container', index.container)}>
          <RouterView routes={child} />
        </section>
      </div>
    );
  }
}

export default Index;
